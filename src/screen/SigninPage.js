import React from 'react';
import {
  Image,
  Text,
  TextInput,
  Touchable,
  TouchableOpacity,
  View,
} from 'react-native';
const SigninPage = ({navigation}) => {
  return (
    <View style={{flex: 1}}>
      <Image
        style={{alignSelf: 'center', marginTop: 140}}
        source={require('../img/Group502.png')}></Image>

      <View
        style={{
          width: 273,
          height: 43,
          backgroundColor: '#FFFFFF',
          alignSelf: 'center',
          marginTop: 17,
        }}>
        <TextInput style={{marginLeft: 17.63}} placeholder="Username" />
      </View>
      <View
        style={{
          width: 273,
          height: 43,
          backgroundColor: '#FFFFFF',
          alignSelf: 'center',
          marginTop: 17,
          justifyContent: 'center',
          flexDirection: 'row',
        }}>
        <TextInput style={{marginLeft: 12.63}} placeholder="*********" />
        <Image
          style={{marginTop: 15, marginLeft: 150}}
          source={require('../img/Group2.png')}></Image>
      </View>
      <TouchableOpacity
        style={{
          width: 273,
          height: 43,
          backgroundColor: '#2E3283',
          alignSelf: 'center',
          marginTop: 17,
          justifyContent: 'center',
        }}>
        <Text style={{color: '#FFFFFF', fontSize: 16, alignSelf: 'center'}}>
          Login
        </Text>
      </TouchableOpacity>
      <Text style={{alignSelf: 'center', color: '#2E3283', marginTop: 17}}>
        Forget Password
      </Text>
      <Image
        style={{alignSelf: 'center', marginTop: 30}}
        source={require('../img/Group499.png')}></Image>
      <View style={{flexDirection: 'row', alignSelf: 'center'}}>
        <Text style={{alignSelf: 'center', marginTop: 33, marginRight: 30}}>
          APP Version
        </Text>

        <Text style={{alignSelf: 'center', marginTop: 33}}>2.8.3</Text>
      </View>
      <View
        style={{
          width: 393,
          backgroundColor: '#FFFFFF',
          height: 30,
          position: 'absolute',
          bottom: 0,
          justifyContent:'center',
          alignItems:'center',
          flexDirection:'row'
        }}>
        <Text style={{}}>
          Already have account?
        </Text>
          <TouchableOpacity onPress={() =>navigation.navigate('Signup')}>
            <Text style={{}}>Signup</Text>
          </TouchableOpacity>
      </View>
    </View>
  );
};
export default SigninPage;
