import { View, Text } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { NavigationContainer } from '@react-navigation/native'
import SigninPage from './SigninPage'
import SignupPage from './SignupPage'

const Stack = createNativeStackNavigator();
export default function Rooting(){
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {/* <Stack.Screen name="Home" component={HomeScreen}/> */}
        <Stack.Screen name="Signin" component={SigninPage} />
        <Stack.Screen name="Signup" component={SignupPage} />
      </Stack.Navigator>
    </NavigationContainer>
    // <NavigationContainer>
    //     <Stack.Navigator>
    //         <Stack.Screen
    //         name ="Signin"
    //         component={SigninPage}
    //         />
    //         <Stack.Screen
    //         name ="Signup"
    //         component={SignupPage}
    //         />
    //     </Stack.Navigator>
    // </NavigationContainer>
  )
}
function HomeScreen() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
      </View>
    );
  }

// export default Rooting