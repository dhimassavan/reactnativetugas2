import React from 'react';
import {
  Image,
  Text,
  TextInput,
  Touchable,
  TouchableOpacity,
  View,
} from 'react-native';
const SignupPage = ({navigation}) => {
  return (
    <View style={{flex:1}}>
      <Image
        style={{alignSelf: 'center', marginTop: 140}}
        source={require('../img/Group501.png')}></Image>
      <View
        style={{
          width: 273,
          height: 43,
          backgroundColor: '#FFFFFF',
          alignSelf: 'center',
          marginTop: 33,
        }}>
        <TextInput style={{marginLeft:17.63}}  placeholder="Name" />
      </View>
      <View
        style={{
          width: 273,
          height: 43,
          backgroundColor: '#FFFFFF',
          alignSelf: 'center',
          marginTop: 17,
        }}>
        <TextInput style={{marginLeft:17.63}} placeholder="Email" />
      </View>
      <View
        style={{
          width: 273,
          height: 43,
          backgroundColor: '#FFFFFF',
          alignSelf: 'center',
          marginTop: 17,
        }}>
        <TextInput style={{marginLeft:17.63}}  placeholder="Phone" />
      </View>
      <View
        style={{
          width: 273,
          height: 43,
          backgroundColor: '#FFFFFF',
          alignSelf: 'center',
          marginTop: 17,
        }}>
        <TextInput style={{marginLeft:17.63}}  placeholder="Password" />
      </View>
      <TouchableOpacity style={{ width: 273,
          height: 43,
          backgroundColor: '#2E3283',
          alignSelf: 'center',
          marginTop: 17,
          justifyContent:'center'}}>
        <Text style={{color:'#FFFFFF',fontSize: 16,alignSelf:'center'}}>
          Sign Up
        </Text>
      </TouchableOpacity>
        <View style={{justifyContent:'center',width:393,backgroundColor:'#FFFFFF',height:30, position:'absolute', bottom:0,flexDirection:'row'}}>
        <Text style={{}}>
        Already have account? 
        </Text>
        <TouchableOpacity onPress={()=> navigation.navigate('Signin')}>
         <Text>
          Login?
         </Text>
        </TouchableOpacity>
        </View>
    </View>
  );
};
export default SignupPage;
