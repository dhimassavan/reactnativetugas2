/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import SigninPage from './src/screen/SigninPage.js';

AppRegistry.registerComponent(appName, () => App);
